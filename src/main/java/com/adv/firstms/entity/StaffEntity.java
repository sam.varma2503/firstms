package com.adv.firstms.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity(name = "staff")
public class StaffEntity {

	@Id
	private long id;
	
	@Column(name = "name")
	private String name;
	
	private String email;

	public StaffEntity() {
	}
	
	public StaffEntity(int id, String name, String email) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "StaffEntity [id=" + id + ", name=" + name + ", email=" + email + "]";
	}
}
