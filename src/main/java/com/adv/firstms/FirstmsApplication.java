package com.adv.firstms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "First Microservice API's", version = "2.0", description = "Has Information about Employees, First Controller"))
public class FirstmsApplication {

	public static void main(String[] args) {
		SpringApplication.run(FirstmsApplication.class, args);
	}
}
