package com.adv.firstms.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FirstController {

	@GetMapping(value = "/helloworld")
	public ResponseEntity<String> helloWorld() {
		return new ResponseEntity<>("Hello World, Welcome to MS", HttpStatus.OK);
	}
}
