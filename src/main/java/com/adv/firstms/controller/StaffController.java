package com.adv.firstms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.adv.firstms.dto.StaffDto;
import com.adv.firstms.service.StaffService;

@RestController
public class StaffController {

	@Autowired
	@Qualifier(value = "staffServiceImpl")
	private StaffService staffService;
	
	@GetMapping(value = "/staffs")
	public ResponseEntity<List<StaffDto>> getAllStaff() {
		return new ResponseEntity<>(staffService.getAllStaff(), HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/staffs/{staffid}")
	public ResponseEntity<List<StaffDto>> getAllStaff(@PathVariable(value = "staffid") long staffId) {
		staffService.deleteStaff(staffId);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
